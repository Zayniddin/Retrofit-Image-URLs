package net.dasturlash.retrofitimageurls.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.squareup.picasso.Picasso;

import net.dasturlash.retrofitimageurls.Models.ImageModel;
import net.dasturlash.retrofitimageurls.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zayniddin on 19-Oct-17.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    private View view;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Picasso.with(view.getContext())
                .load(ImageModel.URLS[position])
                .into(holder.imgView);
    }

    @Override
    public int getItemCount() {
        return ImageModel.URLS.length;
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgView;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.imgView = (ImageView) itemView.findViewById(R.id.imgContainer);
        }
    }
}
