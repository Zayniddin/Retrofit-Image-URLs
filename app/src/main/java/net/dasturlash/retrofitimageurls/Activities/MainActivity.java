package net.dasturlash.retrofitimageurls.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import net.dasturlash.retrofitimageurls.Adapters.RecyclerViewAdapter;
import net.dasturlash.retrofitimageurls.R;
import net.dasturlash.retrofitimageurls.Models.ImageModel;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<ImageModel> myModel;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new GridLayoutManager(getBaseContext(),2));
        recyclerView.setAdapter(new RecyclerViewAdapter());

    }
}
